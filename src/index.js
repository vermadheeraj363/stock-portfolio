import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import store from './store';
// Importing the Bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css'; 

import {Provider} from 'react-redux';

ReactDOM.render(
    <React.StrictMode>
        { /* Provide State to Any Component */ } 
        <Provider store={store}>
            <App />
        </Provider>
    </React.StrictMode> , 
    document.getElementById('root')
);
