const initialState = {
    userDetails: {},
}
const loginReducers = (state = initialState, action) => {
    switch (action.type) {
        case 'LOGGEDINDATA':
            return {...state, userDetails: action.payload};
        default:
            return state;
    }
};

export default loginReducers;