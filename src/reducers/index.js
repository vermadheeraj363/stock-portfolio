import loginReducers from './login-reducer';
import myStockReducers from './mystock-reducer';

import { combineReducers } from 'redux';
const rootReducer = combineReducers({
    loginReducers,
    myStockReducers
});

export default rootReducer;