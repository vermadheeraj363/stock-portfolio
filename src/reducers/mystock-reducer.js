const initialState = {
    userStocks: [],
}
const myStockReducers = (state = initialState, action) => {
    switch (action.type) {
        case 'USERSTOCKDATA':
            return {...state, userStocks: action.payload};
        default:
            return state;
    }
};

export default myStockReducers;