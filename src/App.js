import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Login from './views/login/Login';
import Home from './views/home/Home';
import PrivateAuthRoute from './routes/PrivateAuthRoute';
import './App.css';


function App() {
  
  return (
    <Router>
    <div>
      <Routes>
        <Route exact path="/" element={<Login/>}/>
        <Route exact path="/home" element={
          <PrivateAuthRoute>
            <Home/>
          </PrivateAuthRoute>
        }/>
      </Routes>
    </div>
  </Router>
  );
}

export default App;
