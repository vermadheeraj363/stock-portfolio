export const stockPriceData =[
	{
		"date":"2022-02-10",
		"price":2500,
	},
	{
		"date":"2022-02-11",
		"price":1500,
	},
	{
		"date":"2022-02-12",
		"price":2000,
	},
	{
		"date":"2022-02-13",
		"price":1800,
	},
	{
		"date":"2022-02-14",
		"price":1100,
	},
	{
		"date":"2022-02-15",
		"price":2500,
	},
	{
		"date":"2022-02-16",
		"price":1200,
	},
];
