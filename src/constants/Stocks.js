export const stockData =[
    {
        "asset_id": "BTC",
        "name": "Bitcoin",
        "price_usd": 500
    },
    {
        "asset_id": "USD",
        "name": "US Dollar",
        "price_usd": 250
    },
    {
        "asset_id": "PLN",
        "name": "Zloty",
        "price_usd": 10,
    },
    {
        "asset_id": "EUR",
        "name": "Euro",
        "price_usd": 45,
    },
    {
        "asset_id": "CNY",
        "name": "Yuan Renminbi",
        "price_usd":10
    },
    {
        "asset_id": "JPY",
        "name": "Yen",
        "price_usd": 7,
    },
    {
        "asset_id": "AUD",
        "name": "Australian Dollar",
        "price_usd": 62,
    },
    {
        "asset_id": "CHF",
        "name": "Swiss Franc",
        "price_usd": 147,
    },
    {
        "asset_id": "SEK",
        "name": "Swedish Krona",
        "price_usd": 59,
    },
    {
        "asset_id": "GBP",
        "name": "Pound Sterling",
        "price_usd": 24,
        
    }
];