const savedUserStock= (stockDetails) => ({
    type: 'USERSTOCKDATA',
    payload: stockDetails
});

export default savedUserStock;