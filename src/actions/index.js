import savedUserLogin from './login-action';
import savedUserStock from './mystock-action';

const Actions = {
    savedUserLogin,
    savedUserStock
};

export default Actions;

