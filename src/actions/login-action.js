const savedUserLogin= (loginDetails) => ({
    type: 'LOGGEDINDATA',
    payload: loginDetails
});

export default savedUserLogin;