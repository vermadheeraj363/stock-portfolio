import React from 'react';
import { Navigate } from "react-router-dom";
import {useSelector} from 'react-redux';

const PrivateAuthRoute = ({ children }) => {
    const storedUseDetails = useSelector((state) => state.loginReducers.userDetails)

    function isLogin() {
        if(Object.keys(storedUseDetails).length>0){
           return true;
        }
        return false;
    }
    if (isLogin()===false) {
        return <Navigate to="/" replace />;
    }

    return children;
};

export default PrivateAuthRoute;