import React from "react";
import {Row,Col,Button} from 'react-bootstrap';

const StockItem = ({item,handleBuyNowClick,handleViewChart}) => { 
  return (
    <div className='stock-item-list'>
        <Row>
          <Col sm={8} className="">
              <div className='cu-item-name'>{item.name} ( {item.asset_id} ) - Price : {item.price_usd} </div>
          </Col>
          <Col sm={2} className="text-right">
            <Button type="button" onClick={() => handleBuyNowClick(item)} className="btn btn-primary btn-sm" >Buy Now</Button>
          </Col>
          <Col sm={2} className="text-left">
            <Button type="button" className="btn btn-warning btn-sm" onClick={() => handleViewChart(item)}> View Chart</Button>
          </Col>
        </Row>
    </div>
  );
};

export default StockItem;