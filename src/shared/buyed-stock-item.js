import React from "react";
import {Row,Col,Button} from 'react-bootstrap';

const BuyedStockItem = ({item,handleViewChart}) => { 
  return (
    <div className='stock-item-list'>
        <Row>
          <Col sm={6} className="">
              <div className='cu-item-name'>{item.name} ( {item.asset_id} ) - Price : {item.price_usd} </div>
          </Col>
          <Col sm={2} className="text-right">
            <Button type="button" className="btn btn-success btn-sm min-width-100"> QTY : {item.qty}</Button>
          </Col>
          <Col sm={2.5} className="text-right">
            <Button type="button" className="btn btn-danger btn-sm min-width-125"> Invested : {item.invested}</Button>
          </Col>
          <Col sm={2} className="text-left">
            <Button type="button" className="btn btn-warning btn-sm" onClick={() => handleViewChart(item)}> View Chart</Button>
          </Col>
        </Row>
    </div>
  );
};

export default BuyedStockItem;