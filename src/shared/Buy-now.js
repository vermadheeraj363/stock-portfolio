import React,{useState} from "react";
import {Row,Col,Button,Modal,Form} from 'react-bootstrap';
import {useDispatch,useSelector} from 'react-redux';
import Actions from '../actions/index'
import {toast} from 'react-toastify'; 

const BuyNow = ({selectedStockData,showHideBuyModal,handleCloseBuyModal}) => { 
    toast.configure() 
    const disptach=useDispatch();
    //getting buyed stocks and used to push new buyed stock
    const storedUserStocks = useSelector((state) => state.myStockReducers.userStocks)
  
    const [buyasset, setBuyAsset] = useState({
        "asset_id":"",
        "name":"",
        "price_usd":0,
        "qty":0,
        "invested":0
    });
    
    //setting qty of stock and to calculate to pay amount
    const setQty = (e) => {
        let myAsset=buyasset;
        myAsset.asset_id=selectedStockData.asset_id;
        myAsset.name=selectedStockData.name;
        myAsset.price_usd=selectedStockData.price_usd;
        myAsset.qty=e.target.value;
        myAsset.invested=parseFloat(myAsset.qty)*parseFloat(myAsset.price_usd);
        setBuyAsset({...myAsset});
    }

    //handled actual buy stock
    const handleBuyNow = () => {
        if(buyasset.qty!=='' && buyasset.qty!=='0' && parseFloat(buyasset.qty)>parseFloat(0)){
            storedUserStocks.push(buyasset);
            disptach(Actions.savedUserStock([...storedUserStocks]));
            handleCloseBuyModal(false);
            toast.success(buyasset.name+' Buyed Successfully');
        }else{
            toast.error('Invalid Qty Entered');
        }
    }

    return (
        <Modal show={showHideBuyModal} onHide={handleCloseBuyModal} animation={false}>
            <Modal.Header closeButton>
                <Modal.Title>Buy {selectedStockData.name} ({selectedStockData.asset_id})</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Row>
                    <Col sm={4}>
                        <div className="form-group">
                            <Form.Label>Price</Form.Label>
                            <Form.Control 
                                type="number" 
                                className="form-control" 
                                readOnly={true}
                                value={selectedStockData.price_usd}
                            />
                        </div>
                    </Col>
                    <Col sm={4}>
                        <div className="form-group">
                            <Form.Label>Qty</Form.Label>
                            <Form.Control 
                                type="number" 
                                className="form-control" 
                                value={buyasset.qty}
                                onChange={event => setQty(event)}
                            />
                        </div>
                    </Col>
                    <Col sm={4}>
                        <div className="form-group">
                            <Form.Label>To Pay</Form.Label>
                            <Form.Control 
                                type="number" 
                                className="form-control" 
                                readOnly={true}
                                value={buyasset.invested}
                            />
                        </div>
                    </Col>
                </Row>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleCloseBuyModal}>
                    Close
                </Button>
                <Button variant="primary" onClick={handleBuyNow}>
                    Buy Now
                </Button>
            </Modal.Footer>
        </Modal>  
    );
};

export default BuyNow;