import React from "react";

const DangerAlert = ({ errortext }) => { 
  return (
    <div className="alert alert-danger">
        {errortext}
    </div>
  );
};

export default DangerAlert;