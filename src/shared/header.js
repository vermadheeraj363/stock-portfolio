import React from "react";
import { useNavigate } from 'react-router-dom';
import {Row,Col,Button,Card} from 'react-bootstrap';
import {toast} from 'react-toastify'; 
import {useSelector,useDispatch} from 'react-redux';
import Actions from '../actions/index'

const Header = () => { 
  toast.configure() 
  const disptach=useDispatch();
  const navigate = useNavigate();
  const storedUseDetails = useSelector((state) => state.loginReducers.userDetails)

  function handleLogout (e) {
    disptach(Actions.savedUserLogin({}));
    toast.success('Logout Successfull');
    navigate("/");
  }
  return (
    <div>
        <Row>
            <Col sm={12}>
                <Card className={"mt-10 mb-2 p-2"}>  
                    <Row>
                      <Col sm={9}>
                        <h5>Welcome To Stock Portfolio</h5>
                      </Col>
                      <Col sm={2}>
                        <h5 className="text-red">Mr. {storedUseDetails.USER_NAME}</h5>
                      </Col>
                      <Col sm={1} className='text-right'>
                        <Button type="button" onClick={handleLogout} className="btn btn-primary btn-sm btn-block" >Logout</Button>
                      </Col>
                    </Row>
                </Card>
            </Col>
            
        </Row>
    </div>
  );
};

export default Header;