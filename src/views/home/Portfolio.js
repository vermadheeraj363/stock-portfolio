import React,{useState} from "react";
import BuyedStockItem from "../../shared/buyed-stock-item";
import DangerAlert from "../../shared/danger-alert";
import {useSelector} from 'react-redux';
import Chart from './Chart';
import { stockData } from "../../constants/Stocks";
import {Row,Col,Button} from 'react-bootstrap'

const Portfolio = () => { 
    //getting buyed stocks
    const storedUserStocks = useSelector((state) => state.myStockReducers.userStocks)
    //manage show hide charts 
    const [showHideChart, setShowHideChart] = useState(false);
    //manage selected stock 
    const [selectedStockData, setSelectedStockData] = useState({});

    //handling click for the close chart button
    const handleCloseChart = () => {
        setShowHideChart(false);
    }

    //handling click for the chart view button
    const handleViewChart = (item) => {
        let findStock=stockData.filter((p)=>p.asset_id === item.asset_id)[0];
        setShowHideChart(true);
        setSelectedStockData(findStock);
    }

    return (
    <>
        {showHideChart?
            <>
            <Chart selectedStockData={selectedStockData} />
            <Row>
                <Col sm={12} className='text-right mt-10'>
                    <Button type="button" className="btn btn-danger btn-md" onClick={() => handleCloseChart()}> Close</Button>
                </Col>
            </Row>
            </>
        :
            <div className="right-tab-height">
                {storedUserStocks.length>0?
                    <>
                        {storedUserStocks.map((item,index) => (
                            <div key={index.toString()}>
                                <BuyedStockItem
                                    item={item}
                                    handleViewChart={handleViewChart}
                                />
                            </div>
                        ))}
                    </>
                :
                    <DangerAlert errortext={'Portfolio list is empty'} />
                }
            </div> 
        }
                                   
    </>
  );
};

export default Portfolio;
