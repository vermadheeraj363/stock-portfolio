import React,{useState} from 'react';
import { CanvasJSChart } from 'canvasjs-react-charts';
import { stockPriceData } from "../../constants/StockPriceData";
import {Row,Col,Form,Button} from 'react-bootstrap';


const Chart = ({selectedStockData}) => {
    const [stockPrices,setstockPrices]=useState(stockPriceData);

    const [filterData,setFilterData]=useState({
        "FROMDATE":"2022-02-01",
        "TODATE":"2022-02-16",
    });

    const setValue = (value,key) => {
        let filters=filterData;
        filters[key]=value;
        setFilterData({...filters});
    }

    const handleSearch=()=>{
        let filteredPrices = stockPriceData.filter((item: any) =>
            new Date(item.date+"T00:00:00Z").getTime() >= new Date(filterData.FROMDATE+"T00:00:00Z").getTime() && new Date(item.date+"T00:00:00Z").getTime() <= new Date(filterData.TODATE+"T00:00:00Z").getTime()
        );
        setstockPrices(filteredPrices);
    }
   
    return (
        <>
            <Row>
                <Col sm={4}>
                    <div className="form-group">
                        <Form.Label>From Date</Form.Label>
                        <Form.Control 
                            type="date" 
                            className="form-control" 
                            placeholder="Enter password" 
                            value={filterData.FROMDATE}
                            onChange={event => setValue(event.target.value,'FROMDATE')}
                        />
                    </div>
                </Col>
                <Col sm={4}>
                    <div className="form-group">
                        <Form.Label>To Date</Form.Label>
                        <Form.Control 
                            type="date" 
                            className="form-control" 
                            placeholder="Enter password" 
                            value={filterData.TODATE}
                            onChange={event => setValue(event.target.value,'TODATE')}
                        />
                    </div>
                </Col>
                <Col sm={4}>
                    <Button type="button" onClick={handleSearch} className="btn btn-info m-t-35 btn-sm btn-block" >Search</Button>
                </Col>
                
            </Row>
            <CanvasJSChart
                options={ {
                    animationEnabled: true,
                    title:{
                        text: "Graph For "+selectedStockData.name
                    },
                    axisX: {
                        valueFormatString: "DD-MM-YY"
                    },
                    axisY: {
                        title: "Prices (in USD)",
                        prefix: "$"
                    },
                    data: [{
                        yValueFormatString: "$#,###",
                        xValueFormatString: "DD-MM-YY",
                        type: "spline",
                        dataPoints: stockPrices.map(stockPrices => ({
                            x: new Date(stockPrices.date),
                            y: stockPrices.price,
                        }))
                    }]
                } }
            />
        </>
    );
};

export default Chart;