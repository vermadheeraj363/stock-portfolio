import React,{useState} from "react";
import {Button,Row,Col} from 'react-bootstrap'
import Chart from './Chart';
import { stockData } from "../../constants/Stocks";
import StockItem from "../../shared/stock-item";
import BuyNow from "../../shared/Buy-now";

const Portfolio = () => { 
    //manage show hide charts 
    const [showHideChart, setShowHideChart] = useState(false);
    //manage selected stock 
    const [selectedStockData, setSelectedStockData] = useState({});
    //manage show hide buy now popup 
    const [showHideBuyModal, setShowHideBuyModal] = useState(false);

    //handling click for the close chart button
    const handleCloseChart = () => {
        setShowHideChart(false);
    }

    //handling click for the close buy now popup
    const handleCloseBuyModal =() => {
        setShowHideBuyModal(false);
    }

    //handling click for the stock buy popup
    const handleBuyNowClick = (item) => {
        let findStock=stockData.filter((p)=>p.asset_id === item.asset_id)[0];
        findStock.qty=0;
        findStock.invested=0;
        setSelectedStockData(findStock);
        setShowHideBuyModal(true);
    }

    //handling click to open the char view
    const handleViewChart = (item) => {
        let findStock=stockData.filter((p)=>p.asset_id === item.asset_id)[0];
        setSelectedStockData(findStock);
        setShowHideChart(true);
        
    }

    return (
    <>
        {showHideChart?
            <>
                <Chart selectedStockData={selectedStockData} />
                <Row>
                    <Col sm={12} className='text-right mt-10'>
                        <Button type="button" className="btn btn-danger btn-md" onClick={() => handleCloseChart()}> Close</Button>
                    </Col>
                </Row>
            </>
        :
        <>
            <div className="right-tab-height">
                {stockData.map((item,index) => (
                    <div key={index.toString()}>
                        <StockItem
                            item={item}
                            handleBuyNowClick={handleBuyNowClick}
                            handleViewChart={handleViewChart}
                        />
                    </div>
                ))}       
            </div>       
            <BuyNow
                selectedStockData={selectedStockData}
                showHideBuyModal={showHideBuyModal}
                handleCloseBuyModal={handleCloseBuyModal}
            />
        </>
        }
    </>
  );
};

export default Portfolio;
