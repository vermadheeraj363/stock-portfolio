import React from "react";
import {Container,Tab,Row,Col,Nav,Card} from 'react-bootstrap';
import Header from "../../shared/header";
import Portfolio from "./Portfolio";
import Stock from "./Stock";
import { verticalTabsData } from "../../constants/VerticalTabsHome";

function Home() {
   return (
    <>
        <Container>
           <Header />
            <div>
                <Card className={"mb-2 p-2"}>  
                    <Tab.Container id="left-tabs" defaultActiveKey="stocks">
                        <Row>
                            <Col sm={3}>
                                <Nav variant="pills" className="flex-column ml-0 mr-0">
                                    {verticalTabsData.map((item,index) => (
                                        <Nav.Item key={index}>
                                            <Nav.Link eventKey={item.KEY}>{item.VALUE}</Nav.Link>
                                        </Nav.Item>
                                    ))}
                                </Nav>
                            </Col>
                            <Col sm={9}>
                                <Tab.Content>
                                    {verticalTabsData.map((item,index) => (
                                        <Tab.Pane key={index} eventKey={item.KEY}>
                                            {item.KEY==='stocks'? <Stock/>: <Portfolio />}
                                        </Tab.Pane>
                                    ))}
                                </Tab.Content>
                            </Col>
                        </Row>
                    </Tab.Container>
                </Card>
            </div>
        </Container>
    </>
  );
}

export default Home;
