import React,{useState,useEffect} from "react";
import { useNavigate } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import {toast} from 'react-toastify'; 
import { userData } from "../../constants/Users";
import {useSelector, useDispatch} from 'react-redux';
import Actions from '../../actions/index'


function Login() {
    toast.configure() 
    const navigate = useNavigate();
    const [email,setEmail]=useState('');
    const [pass,setPass]=useState('');
    const storedUseDetails = useSelector((state) => state.loginReducers.userDetails)
    const disptach=useDispatch();

     useEffect(() => {
        // checking if already logged in
        if(Object.keys(storedUseDetails).length>0){
            navigate("/home");
        }
    }, [])

    // validating user for login
    function validateLogin (e) {
        e.preventDefault();
        if(email===""){
            toast.error('Please Enter Email Id');
            return false;
        }
        if(pass===""){
            toast.error('Please Enter Password');
            return false;
        }
        let findUser=userData.filter((p)=>p.USER_EMAIL === email && p.USER_PASS === pass)[0];
        if(typeof findUser!="undefined"){
            //setting user data
            disptach(Actions.savedUserLogin(findUser));
            toast.success('Login Successfull');
            navigate("/home");
        }else{
            toast.error('Invalid Credentials');
        }
    }

    return (
    <>
        <Container>
            <div className="login-card" >
                <h3 className="text-center">STOCK PORTFOLIO</h3>
                <div className="form-group">
                    <Form.Label>Email</Form.Label>
                    <Form.Control 
                        type="email" 
                        className="form-control" 
                        placeholder="Enter email" 
                        onChange={event => setEmail(event.target.value)}
                    />
                </div>
                <div className="form-group">
                    <Form.Label>Password</Form.Label>
                    <Form.Control 
                        type="password" 
                        className="form-control" 
                        placeholder="Enter password" 
                        onChange={event => setPass(event.target.value)}
                    />
                </div>
                <Button type="button" onClick={validateLogin} className="btn btn-info btn-block" >Login</Button>
            </div>
        </Container>
    </>
  );
}

export default Login;
