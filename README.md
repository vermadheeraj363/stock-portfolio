# Stock Portfolio

An application used to buy stocks with chart pricing and portfolio

## Technologies

I used React, Redux

## Installation

Clone down this repository. You will need node and npm installed globally on your machine.

```bash
git clone https://gitlab.com/vermadheeraj363/stock-portfolio.git
cd stock-portfolio
npm install
npm start
To Visit App:
http://localhost:3000
Email : johndoe@gmail.com
Pass : 123456
```

## Project Status

This project is currently in development. Users can login view their portfolio can buy slots and also can view chart representation.

## Project Future Enhancements
1.We can use any backend service to load vast data for stock in our project using pagination , We can do it from frontend for now but again loading all data at once and then creating pagination on frontend will be the same.

2.For Validating user we can use some backend service to validate and create some authentication like jwt (Json Web Token) so that after login user can be validated on each request

3.Stock day wise prices are fixed with json file it can be get dynamic using some backend service 

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.